function arrayViewList(arr, elem = 'body') {
    const ulElem = document.querySelector(`${elem}`);
    ulElem.insertAdjacentHTML('afterbegin', '<ul></ul>');
    const ul = document.querySelector('ul');

    for (let el of arr) {
        let li = document.createElement('li');
        ul.append(li);
        li.textContent = el;
    }
}

arrayViewList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], 'div');